*** Settings ***

Documentation     A resource file with reusable variables for General.

Resource    ../server/config/server.robot

*** Variables ***
########################### Browser Variables ###########################

${WIDTH}    1366
${HEIGHT}    768


################################# Buttons ###############################
${SINGIN_BUTTON}    xpath=/html/body/div[2]/div/div/div[2]/div/form/footer/button


#################### Credentials ##################### 
${USER_USERNAME}    admin
${USER_PASSWORD}    admin
${USERNAME_INVALID}    NONE    
${PASSWORD_INVALID}    ADMIN

############################### Login Page ##############################  
${LOGIN_INPUT_FIELD}    xpath=/html/body/div[2]/div/div/div[2]/div/form/fieldset/section[1]/label[2]/input
${PASSWORD_INPUT_FIELD}    xpath=/html/body/div[2]/div/div/div[2]/div/form/fieldset/section[2]/label[2]/input
${CHECK_REMEMBER_FIELD}    xpath=/html/body/div[2]/div/div/div[2]/div/form/fieldset/section[3]/label/input/i    


################################# Labels ################################
${10_LABEL}    10


################################# Navbar ################################ 
${ADVANCED_OTA_FIELD}                        admconsoleprovisioning-link 
${DROPDOWN_FIELD}                            adminconsole-link
${ELECTRICAL_PROFILE_DROPDOWN_MENU_FIELD}    electricalprofile-link
${NAVBAR_FIELD}                              css=.search
${UICC_SEARCH_DROPDOWN_MENU_FIELD}           uicc-link


########################### Global Variable ############################# 
# Column List
@{COLUMN_ELEMENTS_LIST}
