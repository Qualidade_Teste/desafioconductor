*** Settings ***

Documentation     A resource file with reusable keywords for general

Resource     general-variables.robot

*** Keywords ***
Click UI Button
    [Arguments]                        ${button}
    Focus                              ${button}
    Click Element                      ${button}

Close test browser
    Close all browsers
       
Insert Information In Field
    [Arguments]                         ${field}    ${information}
    Input Text                          ${field}    ${information}

Initial Login Setup
    [Arguments]        ${browser_information}
    Open Browser       ${URL_LOGIN}              ${browser_information}
    Set Window Size    ${WIDTH}                  ${HEIGHT}
    Sleep              ${sleep}

Login App Check Ativo
    [Arguments]                        ${user_value}              ${password_value}
    Input Text                         ${LOGIN_INPUT_FIELD}       ${user_value}
    Input Text                         ${PASSWORD_INPUT_FIELD}    ${password_value}
    Click Element                      ${SINGIN_BUTTON}
    
Validate Information In Field
    [Arguments]    ${field}    ${expected_value}
    Wait Until Element Is Visible    ${field}    ${TIME_SLEEP}
    ${current_value}=    Get Value    ${field}
    Should Be Equal    ${current_value}    ${expected_value} 

Validate Element Is Disable
    [Arguments]    ${element_locator}
    Wait Until Element Is Visible    ${element_locator}    ${TIME_SLEEP}
    Element Should Be Disabled       ${element_locator}
   
Validate Feedback Result Message 
    [Arguments]                    ${expected_message}
    Page Should Contain Element    ${expected_message}   

Validate Contain Element 
    [Arguments]    ${field}    ${expected_value}
    Wait Until Element Is Visible    ${field}    ${TIME_SLEEP}
    ${current_value}=    Get Value    ${field}
    Should Be Equal    ${current_value}    ${expected_value} 
    