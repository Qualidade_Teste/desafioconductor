*** Settings ***

Documentation     A test suite for Login screen.

Library    Selenium2Library
Library    Collections
Library    String
Library    BuiltIn

Resource    ../pageobject/Login-resources.robot
Resource    ../common/login-variables.robot
Resource    ../../../common/general-resources.robot
Resource    ../../../common/general-variable.robot

Test Teardown  Close test browser

*** Test Cases *** 
CT-1
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Steps:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}

CT-3
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup        ${browser}
    \
    \    #Steps:
    \    Login App Check Inativo    ${USER_USERNAME}    ${USER_PASSWORD}

CT-4
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup                 ${browser}
    \
    \    #Steps:
    \    Login App Check Ativo               ${USERNAME_INVALID}    ${USER_PASSWORD}
    \    Validate Feedback Result Message    ${MESSAGE_ERROR}

CT-5
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup    ${browser}
    \    
    \    #Steps:
    \    Login App Check Ativo    ${USER_USERNAME}    ${PASSWORD_INVALID}
    \    Validate Feedback Result Message    ${MESSAGE_ERROR}     
         