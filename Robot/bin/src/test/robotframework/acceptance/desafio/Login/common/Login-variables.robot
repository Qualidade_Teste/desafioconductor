*** Settings ***

Documentation     A resource file with reusable variables for Login screen.

*** Variables ***

######################## Login Waiting Time ########################
${LOGIN_INITIAL_WAIT_TIME}    20

# Result Messages
${MESSAGE_ERROR}    Invalid credentials  

############################## Values ##############################
#${USERNAME_INVALID}    NONE    
#${PASSWORD_INVALID}    ADMIN
