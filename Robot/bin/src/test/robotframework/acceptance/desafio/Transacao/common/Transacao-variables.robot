*** Settings ***

Documentation     A resource file with reusable variables for Transaction screen.

*** Variables ***

######################## Login Waiting Time ########################
${LOGIN_INITIAL_WAIT_TIME}    20

# Result Messages
${MESSAGE_ERRO_FILTER_CLIENT}
${MESSAGE_ERRO_FILTER_VALUE}


############################## Fields ##############################
${CLIENT_FIELD}    cliente
${VALUE_FIELD}    valorTransacao
${MENU_QA}                xpath=/html/body/aside/nav/ul[2]/li/a/span
${MENU_TRANSACTION}            xpath=/html/body/aside/nav/ul[2]/li/ul/li[2]/a/span
${MENU_ADD_TRANSACTION}        xpath=/html/body/aside/nav/ul[2]/li/ul/li[2]/ul/li[1]/a/span
${MENU_LIST_TRANSACTION}       xpath=/html/body/aside/nav/ul[2]/li/ul/li[2]/ul/li[2]/a/span 
${CLIENTE_FIELD_FILTER}      xpath=

############################## Labels ##############################
${LIST_TRANSACTION}    xpath=/html/body/div[2]/div/div/div[2]/div/h1    

${TABLE_TRANSACTION}    xpath=/html/body/div[2]/div/div/form[2]/div/div/div/table/tbody/tr/td[1]
${TABLE_TRANSACTION_2}    xpath=/html/body/div[2]/div/div/form[2]/div/div/div/table/tbody/tr/td[4]


############################## Values ##############################
${NAME_CLIENT_VALID}              TESTE

${VALUE_TRANSACTION_VALID}           1500
${VALUE_TRANSACTION_NEGATIVE}        -1000
${VALUE_TRANSACTION_POSITIVE}        1000
${VALUE_TRANSACTION_EMPTY}              
${VALUE_TRANSACTION_ZERO}    000

${VALUE_ALPHANUMERIC_TRANSACTION}              1234F56
${VALUE_NUMERIC_TRANSACTION}                   123456
${VALUE_ALPHABETIC_TRANSACTION}                WDERFGTYHHHH
${VALUE_SPECIALCARACTER_TRANSACTION}           !@#$%$¨%%%


############################## Buttonn ##############################
${SALVAR_ADD_BUTTON}        botaoSalvar
${CANCELAR_BUTTON}      xpath=/html/body/div[2]/div/div/form[2]/div/div/div/div/div/a

${SEARCH_LIST}              xpath=/html/body/div[2]/div/div/form[2]/div/div/fieldset[2]/div/div/div/input
${VISUALIZE}                xpath=/html/body/div[2]/div/div/form[2]/div/div/div/table/tbody/tr/td[5]/a
