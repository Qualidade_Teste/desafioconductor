*** Settings ***

Documentation     A resource file with reusable keywords and variables for Electrical Profile History screen

Resource    ../common/Transacao-variables.robot
Resource    ../../../common/general-resources.robot
Resource    ../../../common/general-variables.robot

*** Keywords ***
Access Create Transaction Screen
     Click Element    ${MENU_QA}    
     Click Element    ${MENU_TRANSACTION} 
     Click Element    ${MENU_ADD_TRANSACTION}

Access List Transaction Screen
     Click Element    ${MENU_QA}    
     Click Element    ${MENU_TRANSACTION} 
     Click Element    ${MENU_LIST_TRANSACTION}
     
Create Transaction
    [Arguments]    ${expected_client}      ${expected_value}          ${expected_value_input}
    Click Element CheckBox    ${CLIENT_FIELD}    ${expected_client}
    Insert Information In Field      ${VALUE_FIELD}          ${expected_value}
    Validate Information In Field    ${VALUE_FIELD}          ${expected_value_input}
    Click UI Button                  ${SALVAR_ADD_BUTTON}

Validate Details Screen
    Validate Element Is Disable    ${CLIENT_FIELD}
    Validate Element Is Disable    ${VALUE_FIELD}      
    Page Should Contain Element    ${CANCEL_DETAILS_BUTTON}        
    
Validate Cancel Add 
    Click UI Button                     ${CANCELAR_ADD_BUTTON}
    Validate Feedback Result Message    ${LIST_TRANSACTION}

Verify Selected Client
    [Arguments]    ${field}    ${client}
    Wait Until Element Is Visible    ${field}           ${TIME_SLEEP}
    ${current_text}=                 Get Value          ${field}
    Should Be Equal                  ${current_text}    ${client}

Click Element CheckBox
    [Arguments]    ${expected_checkbox}    ${expected_value_checkbox}
    Click Element                    ${expected_checkbox}    
    Sleep                            ${TIME_SLEEP}
    Click Element                    ${expected_value_checkbox}
    Verify Selected Client           ${expected_checkbox}         ${expected_value_checkbox}