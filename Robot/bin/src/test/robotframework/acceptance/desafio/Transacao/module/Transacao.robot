*** Settings ***

Documentation     A test suite for Transação screen.

Library    Selenium2Library
Library    Collections
Library    String
Library    BuiltIn

Resource    ../pageobject/Transacao-resources.robot
Resource    ../common/Transacao-variables.robot
Resource    ../../Cliente/common/Cliente-variables.robot
Resource    ../../Cliente/pageobject/Cliente-resources.robot
Resource    ../../../common/general-resources.robot
Resource    ../../../common/general-variable.robot

Test Teardown  Close test browser

*** Test Cases *** 
CT-116
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Access Create Transaction Screen
    \    
    \    #Steps:
    \    Create Transaction    ${NAME_CLIENT_VALID}    ${VALUE_TRANSACTION_VALID}    ${VALUE_TRANSACTION_VALID}
    \    Validate Details Screen

CT-117
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Access Create Transaction Screen
    \    
    \    #Steps:
    \    Insert Information In Field      ${VALUE_FIELD}          ${VALUE_TRANSACTION_VALID}
    \    Validate Information In Field    ${VALUE_FIELD}          ${VALUE_TRANSACTION_VALID}
    \    Click UI Button                  ${SALVAR_ADD_BUTTON}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_FILTER_CLIENT} 

CT-118
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Access Create Transaction Screen
    \    
    \    #Steps:
    \    Click UI Button                  ${SALVAR_ADD_BUTTON}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_FILTER_CLIENT} 
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_FILTER_VALUE}  
 
CT-119
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Access Create Transaction Screen
    \    
    \    #Steps:
    \    Click Element                    ${CLIENT_FIELD}
    \    Sleep                            ${TIME_SLEEP}
    \    Click Element                    ${NAME_CLIENT_VALID}
    \    Verify Selected Client           ${CLIENT_FIELD}         ${NAME_CLIENT_VALID}
    \    Click UI Button                  ${SALVAR_ADD_BUTTON}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_FILTER_VALUE}    

CT-120
    :FOR    ${browser}                          IN                              @{BROWSERS}
    \    Initial Login Setup                 ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo               ${USER_USERNAME}                ${USER_PASSWORD}
    \    Access Create Client Screen
    \    Sleep                               ${TIME_SLEEP}
    \    Create Client                       ${NAME_CLIENT_VALID}            ${NAME_CLIENT_VALID}         ${CPF_CLIENT_VALID}          ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Access Create Transaction Screen
    \
    \    #Steps:
    \    Create Transaction                  ${NAME_CLIENT_VALID}            ${VALUE_TRANSACTION_ZERO}    ${VALUE_TRANSACTION_ZERO}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_FILTER_VALUE}

CT-121
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup                 ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo               ${USER_USERNAME}        ${USER_PASSWORD}
    \    Access Create Client Screen
    \    Sleep                               ${TIME_SLEEP}
    \    Create Client                       ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}             ${CPF_CLIENT_VALID}              ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Access Create Transaction Screen
    \
    \    #Steps:
    \    Create Transaction                  ${NAME_CLIENT_VALID}    ${VALUE_TRANSACTION_NEGATIVE}    ${VALUE_TRANSACTION_POSITIVE}
    \    Validate Details Screen

CT-122
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup                 ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo               ${USER_USERNAME}        ${USER_PASSWORD}
    \    Access Create Client Screen
    \    Sleep                               ${TIME_SLEEP}
    \    Create Client                       ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}                 ${CPF_CLIENT_VALID}             ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Access Create Transaction Screen
    \
    \    #Steps:
    \    Create Transaction                  ${NAME_CLIENT_VALID}    ${VALUE_ALPHANUMERIC_TRANSACTION}    ${VALUE_NUMERIC_TRANSACTION}
    \    Validate Details Screen
       
CT-127
    :FOR    ${browser}                          IN                              @{BROWSERS}
    \    Initial Login Setup                 ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo               ${USER_USERNAME}                ${USER_PASSWORD}
    \    Access Create Client Screen
    \    Sleep                               ${TIME_SLEEP}
    \    Create Client                       ${NAME_CLIENT_VALID}            ${NAME_CLIENT_VALID}         ${CPF_CLIENT_VALID}          ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Access Create Transaction Screen
    \    Create Transaction                  ${NAME_CLIENT_VALID}    ${VALUE_TRANSACTION_VALID}    ${VALUE_TRANSACTION_VALID}
    \    Validate Details Screen  
    \    Access Create Transaction Screen
    \    
    \    #Steps:
    \    Create Transaction                  ${NAME_CLIENT_VALID}    ${VALUE_TRANSACTION_VALID}    ${VALUE_TRANSACTION_VALID}
    \    Validate Details Screen
    
CT-132
    :FOR    ${browser}                          IN                              @{BROWSERS}
    \    Initial Login Setup                 ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo               ${USER_USERNAME}                ${USER_PASSWORD}
    \    Access Create Client Screen
    \    Sleep                               ${TIME_SLEEP}
    \    Create Client                       ${NAME_CLIENT_VALID}            ${NAME_CLIENT_VALID}         ${CPF_CLIENT_VALID}          ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Sleep                               ${TIME_SLEEP}
    \    Access Create Transaction Screen
    \    Create Transaction                  ${NAME_CLIENT_VALID}    ${VALUE_TRANSACTION_VALID}    ${VALUE_TRANSACTION_VALID}
    \    Validate Details Screen  
    \    Access List Transaction Screen
    \    
    \    #Steps:
    \    Click Element CheckBox    ${CLIENT_FIELD}    ${NAME_CLIENT_VALID}
    \    Click UI Button                 ${SEARCH_LIST}
    \    Validate Contain Element        ${TABLE_TRANSACTION}        ${NAME_CLIENT_VALID}
    \    Validate Contain Element        ${TABLE_TRANSACTION_2}           ${VALUE_TRANSACTION_VALID}

CT-133
    :FOR    ${browser}                          IN                              @{BROWSERS}
    \    Initial Login Setup                 ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo               ${USER_USERNAME}                ${USER_PASSWORD}
    \    Access Create Client Screen
    \    Sleep                               ${TIME_SLEEP}
    \    Create Client                       ${NAME_CLIENT_VALID}            ${NAME_CLIENT_VALID}         ${CPF_CLIENT_VALID}          ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Sleep                               ${TIME_SLEEP}
    \    Access Create Transaction Screen
    \    Create Transaction                  ${NAME_CLIENT_VALID}    ${VALUE_TRANSACTION_VALID}    ${VALUE_TRANSACTION_VALID}
    \    Validate Details Screen  
    \    Access List Transaction Screen
    \    Click Element CheckBox    ${CLIENT_FIELD}    ${NAME_CLIENT_VALID}
    \    Click UI Button                 ${SEARCH_LIST}
    \    Validate Contain Element        ${TABLE_TRANSACTION}        ${NAME_CLIENT_VALID}
    \    Validate Contain Element        ${TABLE_TRANSACTION_2}           ${VALUE_TRANSACTION_VALID}
    \    
    \    #Steps:
    \    Click UI Button    ${VISUALIZE}
    \    Validate Details Screen
    
CT-138
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup                 ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo               ${USER_USERNAME}        ${USER_PASSWORD}
    \    Access Create Client Screen
    \    Sleep                               ${TIME_SLEEP}
    \    Create Client                       ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}                 ${CPF_CLIENT_VALID}             ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Access Create Transaction Screen
    \
    \    #Steps:
    \    Create Transaction                  ${NAME_CLIENT_VALID}    ${VALUE_ALPHABETIC_TRANSACTION}    ${VALUE_TRANSACTION_EMPTY}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_FILTER_VALUE}

CT-139
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup                 ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo               ${USER_USERNAME}        ${USER_PASSWORD}
    \    Access Create Client Screen
    \    Sleep                               ${TIME_SLEEP}
    \    Create Client                       ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}                 ${CPF_CLIENT_VALID}             ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Access Create Transaction Screen
    \
    \    #Steps:
    \    Create Transaction                  ${NAME_CLIENT_VALID}    ${VALUE_SPECIALCARACTER_TRANSACTION}    ${VALUE_TRANSACTION_EMPTY}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_FILTER_VALUE}   