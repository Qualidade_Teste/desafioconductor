*** Settings ***

Documentation     A resource file with reusable variables for Cliente screen.

*** Variables ***

######################## Login Waiting Time ########################
${LOGIN_INITIAL_WAIT_TIME}    20

# Result Messages
${MESSAGE_ERROR}    Invalid credentials 
${MESSAGE_ERRO_FILTER}
${MESSAGE_ERRO_NAME}             xpath=/html/body/div[2]/div/div/form[2]/div/div/fieldset[1]/div/div/div/small
${MESSAGE_ERRO_CPF}              xpath=/html/body/div[2]/div/div/form[2]/div/div/fieldset[2]/div/div/div/small
${MESSAGE_ERRO_BALANCE}          xpath=/html/body/div[2]/div/div/form[2]/div/div/fieldset[3]/div/div/div/small
${MESSAGE_ERRO_CARD_EXPIRING}    xpath=/html/body/div[2]/div/div/form[2]/div/div/fieldset[4]/div/div/div/small

############################## Fields ##############################
${NAME_FIELD}             nome
${CPF_FIELD}              cpf
${ATIVO_FIELD}            status
${BALANCE_FIELD}          saldoCliente
${CARD_EXPIRING_FIELD}    calendario_input
${MENU_QA}                xpath=/html/body/aside/nav/ul[2]/li/a/span
${MENU_CLIENT}            xpath=/html/body/aside/nav/ul[2]/li/ul/li[1]/a/span
${MENU_ADD_CLIENT}        xpath=/html/body/aside/nav/ul[2]/li/ul/li[1]/ul/li[1]/a
${MENU_LIST_CLIENT}       xpath=/html/body/aside/nav/ul[2]/li/ul/li[1]/ul/li[2]/a/span 
${NAME_FIELD_FILTER}      xpath=/html/body/div[2]/div/div/form[2]/div/fieldset/div/div/div[1]/input

############################## Labels ##############################
${LIST_CLIENT}    xpath=/html/body/div[2]/div/div/div[2]/div/h1    

${TABLE_CLIENT}    xpath=/html/body/div[2]/div/div/form[2]/div/div/table/tbody/tr/td[4]
${TABLE_CLIENT_2}    xpath=/html/body/div[2]/div/div/form[2]/div/div/table/tbody/tr/td[1]


############################## Values ##############################
${NAME_CLIENT_VALID}              TESTE
${CPF_CLIENT_VALID}               12345678900
${CPF_CLIENT_2_VALID}             33356787688
${INATIVO_CLIENT}                 Inativo
${ATIVO_CLIENT}                   Ativo
${BALANCE_CLIENT_VALID}           2500000
${BALANCE_CLIENT_NEGATIVE}        -1000
${BALANCE_CLIENT_POSITIVE}        1000
${BALANCE_CLIENT_ZERO}            00
${CARD_EXPIRING_CLIENT_VALID}    20190415
${VALUE_CLIENT_EMPTY}              


${VALUE_ALPHANUMERIC_CLIENT}              1234F567H896Y88J
${VALUE_NUMERIC_CLIENT}                   123456789688
${VALUE_ALPHABETIC_CLIENT}                WDERFGTYHHHH
${VALUE_SPECIALCARACTER_CLIENT}           !@#$%$¨%%%

${VALUE_PARCIAL_NAME}                     TES
${NAME_FILTER_INVALID}                    hgffh
${CARD_EXPIRING_CLIENT_FILTER_INVALID}    002000
${CARD_EXPIRING_CLIENT_FILTER}            042015

############################## Buttonn ##############################
${SALVAR_ADD_BUTTON}        botaoSalvar
${CANCELAR_ADD_BUTTON}      xpath=/html/body/div[2]/div/div/form[2]/div/div/div/div/div/a
${CLEAN_ADD_BUTTON}         botaoLimpar

${UPDATE_DETAILS_BUTTON}    xpath=/html/body
${CANCEL_DETAILS_BUTTON}    xpath=/html/body/div[2]/div/div/form[2]/div/div/div/div/div/a[2]

${SEARCH_LIST}              xpath=/html/body/div[2]/div/div/form[2]/div/fieldset/div/div/div[1]
${VISUALIZE}                xpath=/html/body/div[2]/div/div/form[2]/div/div/table/tbody/tr/td[5]/a[1]/span

