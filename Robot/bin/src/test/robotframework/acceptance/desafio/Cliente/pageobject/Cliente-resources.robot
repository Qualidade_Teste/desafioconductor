*** Settings ***

Documentation     A resource file with reusable keywords and variables for Cliente screen

Resource    ../common/Cliente-variables.robot
Resource    ../../../common/general-resources.robot
Resource    ../../../common/general-variables.robot

*** Keywords ***
Access Create Client Screen
     Click Element    ${MENU_QA}    
     Click Element    ${MENU_CLIENT} 
     Click Element    ${MENU_ADD_CLIENT}

Access List Client Screen
     Click Element    ${MENU_QA}    
     Click Element    ${MENU_CLIENT} 
     Click Element    ${MENU_LIST_CLIENT}
     
Create Client
    [Arguments]                      ${expected_name}          ${expected_name_input}             ${expected_cpf}    ${expected_cpf_input}    ${expected_balance}    ${expected_balance_input}    ${expected_card_expiring}    ${expected_card_expiring_input}
    Insert Information Client    ${expected_name}          ${expected_name_input}             ${expected_cpf}    ${expected_cpf_input}    ${expected_balance}    ${expected_balance_input}    ${expected_card_expiring}    ${expected_card_expiring_input}
    Click UI Button                  ${SALVAR_ADD_BUTTON}

Validate Details Screen
    Validate Element Is Disable    ${NAME_FIELD}
    Validate Element Is Disable    ${CPF_FIELD}      
    Validate Element Is Disable    ${BALANCE_FIELD}
    Validate Element Is Disable    ${CARD_EXPIRING_FIELD}
    Page Should Contain Element    ${UPDATE_DETAILS_BUTTON}
    Page Should Contain Element    ${CANCEL_DETAILS_BUTTON}    
      
Insert Information Client
    [Arguments]                      ${expected_name}          ${expected_name_input}             ${expected_cpf}    ${expected_cpf_input}    ${expected_balance}    ${expected_balance_input}    ${expected_card_expiring}    ${expected_card_expiring_input}
    Insert Information In Field      ${NAME_FIELD}             ${expected_name}
    Validate Information In Field    ${NAME_FIELD}             ${expected_name_input}
    Insert Information In Field      ${CPF_FIELD}              ${expected_cpf}
    Validate Information In Field    ${CPF_FIELD}              ${expected_cpf_input}
    Insert Information In Field      ${BALANCE_FIELD}          ${expected_balance}
    Validate Information In Field    ${BALANCE_FIELD}          ${expected_balance_input}
    Insert Information In Field      ${CARD_EXPIRING_FIELD}    ${expected_card_expiring}
    Validate Information In Field    ${CARD_EXPIRING_FIELD}    ${expected_card_expiring_input}
    
Validate Fields After Clearing             
    Click UI Button    ${CLEAN_ADD_BUTTON}
    Validate Information In Field    ${NAME_FIELD}             ${VALUE_CLIENT_EMPTY}
    Validate Information In Field    ${CPF_FIELD}              ${VALUE_CLIENT_EMPTY}
    Validate Information In Field    ${BALANCE_FIELD}          ${VALUE_CLIENT_EMPTY}
    Validate Information In Field    ${CARD_EXPIRING_FIELD}    ${VALUE_CLIENT_EMPTY}
    
Validate Cancel Add 
    Click UI Button    ${CANCELAR_ADD_BUTTON}
    Validate Feedback Result Message    ${LIST_CLIENT}
