*** Settings ***

Documentation     A test suite for Cliente screen.

Library    Selenium2Library
Library    Collections
Library    String
Library    BuiltIn

Resource    ../pageobject/Cliente-resources.robot
Resource    ../common/Cliente-variables.robot
Resource    ../../../common/general-resources.robot
Resource    ../../../common/general-variable.robot

Test Teardown  Close test browser

*** Test Cases *** 
CT-37
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen
    \    
    \    #Steps:
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Details Screen
    
CT-41
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen
    \    
    \    #Steps:
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${VALUE_CLIENT_EMPTY}    ${VALUE_CLIENT_EMPTY}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_NAME} 

CT-42
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen
    \    
    \    #Steps:
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${VALUE_CLIENT_EMPTY}    ${VALUE_CLIENT_EMPTY}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_CPF}
              
CT-43
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen
    \    
    \    #Steps:
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${VALUE_CLIENT_EMPTY}    ${VALUE_CLIENT_EMPTY}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_BALANCE}
        
CT-44
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen
    \    
    \    #Steps:
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${VALUE_CLIENT_EMPTY}    ${VALUE_CLIENT_EMPTY}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_CARD_EXPIRING}

CT-45
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen
    \    
    \    #Steps:
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${VALUE_CLIENT_EMPTY}    ${VALUE_CLIENT_EMPTY}    ${VALUE_CLIENT_EMPTY}    ${VALUE_CLIENT_EMPTY}    ${VALUE_CLIENT_EMPTY}    ${VALUE_CLIENT_EMPTY}    ${VALUE_CLIENT_EMPTY}    ${VALUE_CLIENT_EMPTY}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_NAME}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_CPF}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_BALANCE}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_CARD_EXPIRING}    

CT-47
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen
    \    
    \    #Steps:
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_ZERO}    ${BALANCE_CLIENT_ZERO}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_BALANCE}
    
CT-48
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen
    \    
    \    #Steps:
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_NEGATIVE}    ${BALANCE_CLIENT_POSITIVE}     ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Details Screen

CT-49
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen
    \    
    \    #Steps:
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${VALUE_ALPHANUMERIC_CLIENT}    ${VALUE_NUMERIC_CLIENT}    ${VALUE_ALPHANUMERIC_CLIENT}    ${VALUE_NUMERIC_CLIENT}    ${VALUE_ALPHANUMERIC_CLIENT}    ${VALUE_NUMERIC_CLIENT}        
    \    Validate Details Screen
    
CT-50
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen
    \    
    \    #Steps:
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}   ${VALUE_ALPHABETIC_CLIENT}    ${VALUE_CLIENT_EMPTY}    ${VALUE_ALPHABETIC_CLIENT}    ${VALUE_CLIENT_EMPTY}    ${VALUE_ALPHABETIC_CLIENT}     ${VALUE_CLIENT_EMPTY}   
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_NAME}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_CPF}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_BALANCE}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_CARD_EXPIRING}

CT-51
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen
    \    
    \    #Steps:
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${VALUE_SPECIALCARACTER_CLIENT}    ${VALUE_CLIENT_EMPTY}    ${VALUE_SPECIALCARACTER_CLIENT}    ${VALUE_CLIENT_EMPTY}    ${VALUE_SPECIALCARACTER_CLIENT}    ${VALUE_CLIENT_EMPTY}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_NAME}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_CPF}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_BALANCE}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_CARD_EXPIRING}

CT-58
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen  
    \    
    \    #Steps:
    \    Insert Information Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Fields After Clearing

CT-59
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen  
    \    
    \    #Steps:
    \    Insert Information Client    ${VALUE_CLIENT_EMPTY}    ${VALUE_CLIENT_EMPTY}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Click UI Button    ${SALVAR_ADD_BUTTON}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_NAME}
    \    Validate Fields After Clearing

CT-66
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen 
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Details Screen
    \    Sleep     ${TIME_SLEEP}
    \    Access Create Client Screen
    \    
    \    #Steps:
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_2_VALID}    ${CPF_CLIENT_2_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Details Screen

CT-67
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen 
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Details Screen
    \    Sleep     ${TIME_SLEEP}
    \    Access Create Client Screen
    \    
    \    #Steps:
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_CPF}

CT-107
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen 
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Details Screen
    \    Sleep     ${TIME_SLEEP}
    \    Access List Client Screen
    \    
    \    #Steps:
    \    Insert Information In Field    ${NAME_FIELD_FILTER}    ${VALUE_PARCIAL_NAME}
    \    Click UI Button     ${SEARCH_LIST}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_FILTER} 

CT-108
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen 
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Details Screen
    \    Sleep     ${TIME_SLEEP}
    \    Access List Client Screen
    \    
    \    #Steps:
    \    Insert Information In Field    ${NAME_FIELD_FILTER}    ${NAME_CLIENT_VALID}
    \    Click UI Button     ${SEARCH_LIST}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_FILTER}      

CT-109
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup      ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo    ${USER_USERNAME}    ${USER_PASSWORD}
    \    Access Create Client Screen 
    \    Sleep     ${TIME_SLEEP}
    \    Create Client    ${NAME_CLIENT_VALID}    ${NAME_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Details Screen
    \    Sleep     ${TIME_SLEEP}
    \    Access List Client Screen
    \    
    \    #Steps:
    \    Insert Information In Field    ${CARD_EXPIRING_FIELD}    ${CARD_EXPIRING_CLIENT_FILTER}
    \    Click UI Button     ${SEARCH_LIST}
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_FILTER}       
 
CT-110
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup             ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo           ${USER_USERNAME}          ${USER_PASSWORD}
    \    Access Create Client Screen
    \    Sleep                           ${TIME_SLEEP}
    \    Create Client                   ${NAME_CLIENT_VALID}      ${NAME_CLIENT_VALID}              ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Details Screen
    \    Sleep                           ${TIME_SLEEP}
    \    Access List Client Screen
    \
    \    #Steps:
    \    Insert Information In Field     ${NAME_FIELD_FILTER}      ${NAME_CLIENT_VALID}
    \    Insert Information In Field     ${CARD_EXPIRING_FIELD}    ${CARD_EXPIRING_CLIENT_FILTER}
    \    Click UI Button                 ${SEARCH_LIST}
    \    Validate Contain Element        ${TABLE_CLIENT_2}         ${NAME_CLIENT_VALID}
    \    Validate Contain Element        ${TABLE_CLIENT}           ${CARD_EXPIRING_CLIENT_VALID}

CT-111
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup             ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo           ${USER_USERNAME}          ${USER_PASSWORD}
    \    Access Create Client Screen
    \    Sleep                           ${TIME_SLEEP}
    \    Create Client                   ${NAME_CLIENT_VALID}      ${NAME_CLIENT_VALID}              ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Details Screen
    \    Sleep                           ${TIME_SLEEP}
    \    Access List Client Screen
    \
    \    #Steps:
    \    Insert Information In Field     ${NAME_FIELD_FILTER}      ${NAME_FILTER_INVALID}
    \    Insert Information In Field     ${CARD_EXPIRING_FIELD}    ${CARD_EXPIRING_CLIENT_FILTER_INVALID}
    \    Click UI Button                 ${SEARCH_LIST}    
    \    Validate Feedback Result Message    ${MESSAGE_ERRO_FILTER} 

CT-110
    :FOR  ${browser}  IN   @{BROWSERS}
    \    Initial Login Setup             ${browser}
    \
    \    #Precondition:
    \    Login App Check Ativo           ${USER_USERNAME}          ${USER_PASSWORD}
    \    Access Create Client Screen
    \    Sleep                           ${TIME_SLEEP}
    \    Create Client                   ${NAME_CLIENT_VALID}      ${NAME_CLIENT_VALID}              ${CPF_CLIENT_VALID}    ${CPF_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${BALANCE_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}    ${CARD_EXPIRING_CLIENT_VALID}
    \    Validate Details Screen
    \    Sleep                           ${TIME_SLEEP}
    \    Access List Client Screen
    \    Insert Information In Field     ${NAME_FIELD_FILTER}      ${NAME_CLIENT_VALID}
    \    Insert Information In Field     ${CARD_EXPIRING_FIELD}    ${CARD_EXPIRING_CLIENT_FILTER}
    \    Click UI Button                 ${SEARCH_LIST}
    \    Validate Contain Element        ${TABLE_CLIENT_2}         ${NAME_CLIENT_VALID}
    \    Validate Contain Element        ${TABLE_CLIENT}           ${CARD_EXPIRING_CLIENT_VALID}
    \    
    \    #Steps:
    \    Click UI Button    ${VISUALIZE}
    \    Validate Details Screen